File naming schema:
```````````````````
   Documentation found here will follow a loose format outlined below. None
 of this is authoritative, nor is it inended fo -ANY- respectable dictation.
 Follow with an air of doubt. Any push will totally be reviewed if inter-
 esting, and will be in some form entertained, mentioned, or even humored over.
 Please blame yourself with commentary and flashy formatting; Basically, so I
 don't have to

 ${git_root}/Program_Name/
  |`- 1_About.txt
  |`- 2_Requirements.[
  |                    Hardware.txt,
  |                    Libraries.txt,
  |                    build_environment.txt, ]
  |`- 3_Install.[
  |               <utiltiy name( i.e., 'lx' for xen project)>.txt
  |               <plugin name>.txt
  |               # Example:
  |               ${git_root}/weechat/3_install.blowfish.ph.txt ]
  |`- 4a_Usage.[txt |
  |              ${git_root}/bind9/4a_Usage.bind9_DoH_Server.txt ]
  |`- 4b_Examples.[txt |
  |              ${git_root}/4b_Examples.etc-firejail-login-users.txt
  |`- 5_Notes.[Warnings.txt | CVE-XXXX.txt | Undocumented_Features.txt ]
  |`- 6_Versions.[txt |
  |                v1.8.9.txt ]
  `-- 7_Help.[
               IRC_Networks.txt
               http-FAQ.txt
               http-wiki.debian.org.txt
               http-blog.blog.domain.tld.txt

   Alot of this should be taken with a grain of salt. The end result, should
 look like it was done with thought.
