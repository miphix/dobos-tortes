Acquire an installation image:
``````````````````````````````
   Visit https://www.debian.org/releases/ to determine the version that best
 suites your needs. After selecting your release, visit: https://www.debian.org\
 /cdimage/archive/ to start drilling a path to your appropriate installation
 image.

 | Note: For more manual/exotic installation methods, you'll want to find the
 |       live images, rather than netboot, or *-DVD-1.iso.

   For example, as of this writting, 12.2.0/ is the most current stable relase,
 as 11.8.0/ is old-stable. Each will have an *-live suffix to the parent dir-
 ectories that will house any live media, over menu driven installers.

   Debian supports a wide range of archectures. Keep in mind such things as
 stability and select the most relevant architecture for your target
 hardware.

   For each parent directory hosting your chosen ISO image, you will find
 SHA[256|512]SUMS[.sign]. Please, download those two files as well. You will
 need them later.
